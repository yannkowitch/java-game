package tic_tac_toe_version2;

public class Player {

	private char symbol;
	private boolean isActiv;
	private boolean  isWinner;

	public Player(char symbol) {
		this.symbol = symbol;
	}

	public char getSymbol() {
		return symbol;
	}
	
	public void move(Board board, int row, int column) {
		board.setGridEntry(row, column, symbol);
		
	}

	public boolean isActiv() {
		return isActiv;
	}

	public void setTurn(boolean isTurn) {
		this.isActiv = isTurn;
	}
	
	

	public boolean isWinner() {
		return isWinner;
	}

	public void setWinner(boolean isWinner) {
		this.isWinner = isWinner;
	}

}
