package tic_tac_toe_version2;

public class Main {

	public static void main(String[] args) {
		

		Board board = new Board(6, 7);
		Player player1 = new Player('X');
		Player player2 = new Player('O');
		GameManager manager = new GameManager(board, player1, player2);

		manager.playGame();
				
	}

}
