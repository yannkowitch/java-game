package tic_tac_toe_version2;

import java.util.Scanner;

public class GameManager {

	private Board board = null;
	private Player player1 = null;
	private Player player2 = null;
	private Player activPlayer;
	

	public GameManager(Board board, Player player1, Player player2) {
		this.board = board;
		this.player1 = player1;
		this.player2 = player2;
		activPlayer = player1;

	}

	private void switchPlayer() {

		activPlayer.setTurn(false);
		if (activPlayer == player1) {
			activPlayer = player2;
			
		} else if (activPlayer == player2) {
			activPlayer = player1;
		}
		activPlayer.setTurn(true);
		System.out.println("it is player's " + activPlayer.getSymbol() + " turn");

	}

	private Player getActivPlayer() {
		return activPlayer;
	}

	private boolean isWinner() {
		// check for 4 across
		char symbol = activPlayer.getSymbol();

		for (int row = 0; row < board.getGrid().length; row++) {
			for (int col = 0; col < board.getGrid()[0].length - 3; col++) {
				if (board.getGrid()[row][col] == symbol && board.getGrid()[row][col + 1] == symbol
						&& board.getGrid()[row][col + 2] == symbol && board.getGrid()[row][col + 3] == symbol) {
					return true;
				}
			}
		}
		// check for 4 up and down
		for (int row = 0; row < board.getGrid().length - 3; row++) {
			for (int col = 0; col < board.getGrid()[0].length; col++) {
				if (board.getGrid()[row][col] == symbol && board.getGrid()[row + 1][col] == symbol
						&& board.getGrid()[row + 2][col] == symbol && board.getGrid()[row + 3][col] == symbol) {
					return true;
				}
			}
		}
		// check upward diagonal
		for (int row = 3; row < board.getGrid().length; row++) {
			for (int col = 0; col < board.getGrid()[0].length - 3; col++) {
				if (board.getGrid()[row][col] == symbol && board.getGrid()[row - 1][col + 1] == symbol
						&& board.getGrid()[row - 2][col + 2] == symbol && board.getGrid()[row - 3][col + 3] == symbol) {
					return true;
				}
			}
		}
		// check downward diagonal
		for (int row = 0; row < board.getGrid().length - 3; row++) {
			for (int col = 0; col < board.getGrid()[0].length - 3; col++) {
				if (board.getGrid()[row][col] == symbol && board.getGrid()[row + 1][col + 1] == symbol
						&& board.getGrid()[row + 2][col + 2] == symbol && board.getGrid()[row + 3][col + 3] == symbol) {
					return true;
				}
			}
		}
		return false;
	}

	public void playGame() {
		Scanner scanner = new Scanner(System.in);
		
		board.display();

		boolean isWinner = false;
		boolean validPlay;
		int column;
		int row;

		System.out.println("Player " + getActivPlayer().getSymbol() + " starts! ");
		// play a turn
		while (isWinner == false) {

			do {
				// display(board);
				System.out.println("choose a row: ");
				row = scanner.nextInt();

				System.out.println("choose a column: ");
				column = scanner.nextInt();

				// validate play
				validPlay = board.isValidPlay(row, column);

			} while (validPlay == false);

			if (validPlay == true) {

				getActivPlayer().move(board, row, column);

				isWinner = isWinner();

				// break;
				board.display();

				if (isWinner == true) {
					System.out.println("we have a winner");
					break;
				} else {
					switchPlayer();
					//System.out.println("Player " + getActivPlayer().getSymbol() + " is playing now ");

				}
			}

		}

		displayGameResult();
	}

	public void displayGameResult() {
		if (activPlayer.isWinner() == true) {
			System.out.println("player " + activPlayer.getSymbol() + " won");

		} else {
			System.out.println("Tie game");
		}
	}
}
