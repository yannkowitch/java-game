package tic_tac_toe_version2;

public class Board {

	private int rows;
	private int columns;
	private char[][] grid;

	public Board(int rows, int columns) {

		this.rows = rows;
		this.columns = columns;
		this.grid = new char[rows][columns];
		fillGrids(' ');

	}

	public char[][] getGrid() {
		return grid;
	}

	public boolean isValidPlay(int row, int column) {

		if (row < 0 || row > rows - 1 || column < 0 || column > columns - 1) {
			if (row < 0 || row > rows - 1) {
				System.out.println("row out of range");
			} else if (column < 0 || column > columns - 1) {
				System.out.println("column out of range");
			}
			return false;
		}

		else if (grid[row][column] != ' ') {
			System.out.println("someone already made a move there!");
			return false;
		} else {
			return true;

		}

	}

	public char getGridEntry(int row, int col) {
		return grid[row][col];
	}

	public void setGridEntry(int row, int col, char value) {
		grid[row][col] = value;

	}

	public int getRows() {
		return rows;
	}

	public int getColumns() {
		return columns;
	}

	public void display() {

		System.out.print(" ");
		System.out.println(" 0 1 2 3 4 5 6");
		System.out.print(" ");
		System.out.println("...............");

		for (int row = 0; row < rows; row++) {
			System.out.print(row);
			System.out.print("|");
			for (int col = 0; col < columns; col++) {
				System.out.print(grid[row][col]);
				System.out.print("|");
			}
			System.out.println();
			System.out.print(" ");
			System.out.println("---------------");
		}

		System.out.println();
	}

	private void fillGrids(char cellValue) {

		for (int row = 0; row < 6; row++) {

			for (int column = 0; column < 7; column++) {

				grid[row][column] = cellValue;
			}

		}
	}

}
