package tic_tac_toe_version1;

import java.util.Scanner;

public class Connect4 {
	public static void main(String[] args) {

		Scanner scanner = new Scanner(System.in);

		char[][] board = new char[6][7];

		// initialize array
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[0].length; col++) {
				board[row][col] = ' ';

			}
		}

		display(board);

		char player = 'X';
		boolean isWinner = false;

		boolean validPlay;

		int column;
		int row;

		System.out.println("Player " + player + " starts! ");
		// play a turn
		while (isWinner == false) {

			do {
				// display(board);
				System.out.println("choose a row: ");
				row = scanner.nextInt();

				System.out.println("choose a column: ");
				column = scanner.nextInt();

				// validate play
				validPlay = isValidPlay(board, row, column);

			} while (validPlay == false);

			if (validPlay == true) {

				playerMove(player, board, row, column);

				isWinner = isWinner(player, board);

				// break;
				display(board);

				if (isWinner == true) {
					System.out.println("we have a winner");
					break;
				} else {
					if (player == 'X') {
						player = 'O';
					} else if (player == 'O') {
						player = 'X';
					}
					System.out.println("Player " + player + " is playing now ");

				}
			}

		}

		displayGameResult(isWinner, player);

	}

	public static void playerMove(char player, char[][] board, int row, int column) {
		board[row][column] = player;
	}

	public static void displayGameResult(boolean isWinner, char player) {
		if (isWinner == true) {
			if (player == 'X') {
				System.out.println("player (X) won");
			} else {
				System.out.println("player (O) won");
			}
		} else {
			System.out.println("Tie game");
		}
	}

	public static void display(char[][] board) {
		System.out.print(" ");
		System.out.println(" 0 1 2 3 4 5 6");
		System.out.print(" ");
		System.out.println("...............");
		for (int row = 0; row < board.length; row++) {
			System.out.print(row);
			System.out.print("|");
			for (int col = 0; col < board[0].length; col++) {
				System.out.print(board[row][col]);
				System.out.print("|");
			}
			System.out.println();
			System.out.print(" ");
			System.out.println("---------------");
		}
		// System.out.println(" 0 1 2 3 4 5 6");
		System.out.println();
	}

	public static boolean isValidPlay(char[][] board, int row, int column) {
		
		if (row < 0 || row > 5 || column < 0 || column > 6) {
			if (row < 0 || row > 5) {
				System.out.println("row out of range");
			} else if (column < 0 || column > 6) {
				System.out.println("column out of range");
			}
			return false;
		}
		else if (board[row][column] != ' ') {
			System.out.println("someone already made a move there!");
			return false;
		} else {
			return true;
		}

	}


	public static boolean isWinner(char player, char[][] board) {
		// check for 4 across
		for (int row = 0; row < board.length; row++) {
			for (int col = 0; col < board[0].length - 3; col++) {
				if (board[row][col] == player && board[row][col + 1] == player && board[row][col + 2] == player
						&& board[row][col + 3] == player) {
					return true;
				}
			}
		}
		// check for 4 up and down
		for (int row = 0; row < board.length - 3; row++) {
			for (int col = 0; col < board[0].length; col++) {
				if (board[row][col] == player && board[row + 1][col] == player && board[row + 2][col] == player
						&& board[row + 3][col] == player) {
					return true;
				}
			}
		}
		// check upward diagonal
		for (int row = 3; row < board.length; row++) {
			for (int col = 0; col < board[0].length - 3; col++) {
				if (board[row][col] == player && board[row - 1][col + 1] == player && board[row - 2][col + 2] == player
						&& board[row - 3][col + 3] == player) {
					return true;
				}
			}
		}
		// check downward diagonal
		for (int row = 0; row < board.length - 3; row++) {
			for (int col = 0; col < board[0].length - 3; col++) {
				if (board[row][col] == player && board[row + 1][col + 1] == player && board[row + 2][col + 2] == player
						&& board[row + 3][col + 3] == player) {
					return true;
				}
			}
		}
		return false;
	}
}
